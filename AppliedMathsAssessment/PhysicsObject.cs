﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace AppliedMathsAssessment
{
    public class PhysicsObject
    {
        // ------------------
        // Data
        // ------------------

        // Transform
        protected Vector3 rotation;
        protected Vector3 position;
        protected Vector3 scale = Vector3.One;

        // Physics
        protected BoundingBox hitBox;
        protected Vector3 velocity;
        protected Vector3 acceleration;
        protected float drag = 0.05f;
        protected Vector3 collisionScale = Vector3.One;
        protected bool isStatic = false;    // Not moved by physics
        protected bool useGravity = false;  // Falls with gravity each frame
        protected bool isTrigger = true;    // Does not trigger physics affects (but can still be sensed with collisions)
        protected float gravityScale = 1f;

        // Previous state
        protected Vector3 positionPrev;
        protected Vector3 velocityPrev;
        protected Vector3 accelerationPrev;

        // Numerical Integration
        public enum IntegrationMethod {
            EXPLICIT_EULER,
            METHOD_TWO,
            METHOD_THREE
        };
        // UPDATE THIS TO TEST TASK 6
        private IntegrationMethod currentIntegrationMethod = IntegrationMethod.EXPLICIT_EULER;

        // ------------------
        // Behaviour
        // ------------------
        public BoundingBox GetHitBox()
        {
            return hitBox;
        }
        // ------------------
        public virtual void UpdateHitBox()
        {
            // Just make a cube hitbox based on the scale
            hitBox = new BoundingBox(-collisionScale * 0.5f, collisionScale * 0.5f);

            // Move to correct position in game world
            hitBox.Min += position;
            hitBox.Max += position;
        }
        // ------------------
        public Vector3 GetPosition()
        {
            return position;
        }
        // ------------------
        public void SetPosition(Vector3 newPosition)
        {
            position = newPosition;
        }
        // ------------------
        public void SetScale(Vector3 newScale)
        {
            scale = newScale;
        }
        // ------------------
        public Vector3 GetGravityVector()
        {
            return new Vector3(0, -9.8f * gravityScale, 0);
        }
        // ------------------
        public virtual void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Update acceleration due to gravity
            if (useGravity)
                acceleration.Y = -9.8f * gravityScale;

            // Store current state before making any modifications
            Vector3 positionCur = position;
            Vector3 velocityCur = velocity;
            Vector3 accelerationCur = acceleration;

            // Update velocity due to drag
            velocity *= (1.0f - drag);

            // Update velocity and position based on acceleration
            // Uses numerical integration (multiple possible methods)
            switch (currentIntegrationMethod)
            {
                case IntegrationMethod.EXPLICIT_EULER:
                    // This method is being deprecated due to stability issues.
                    position += velocity * dt;
                    velocity += acceleration * dt;
                    break;
                    
                ///////////////////////////////////////////////////////////////////
                //
                // CODE FOR TASK 6 SHOULD BE ENTERED HERE
                //
                ///////////////////////////////////////////////////////////////////
                case IntegrationMethod.METHOD_TWO:

                    // Insert method two code here
                    // Change value of currentIntegrationMethod to test this method


                    break;

                case IntegrationMethod.METHOD_THREE:

                    // Insert method three code here
                    // Change value of currentIntegrationMethod to test this method


                    break;

                ///////////////////////////////////////////////////////////////////  
                // END TASK 6 CODE
                ///////////////////////////////////////////////////////////////////  
            }

            // Store current state as previous state
            positionPrev = positionCur;
            velocityPrev = velocityCur;
            accelerationPrev = accelerationCur;

            // Update hitbox
            UpdateHitBox();
        }
        // ------------------
        public virtual void HandleCollision(PhysicsObject other)
        {
            // Don't react with physics if this object is static
            if (isStatic || isTrigger || other.isTrigger)
                return;

            ///////////////////////////////////////////////////////////////////
            //
            // CODE FOR TASK 3 SHOULD BE ENTERED HERE
            //
            ///////////////////////////////////////////////////////////////////

            // OLD VERSION, DEPRECATED
            // Re-write this to account for more realistic collisions using 
            // the incoming angle
            //velocity *= -1;
            // position = positionPrev;

            
            
                             
               
                // Override the current velocity with this new one
                velocity *= velocityPrev;
            

            // Keep track of the ball's old position
            // (We'll need this for our simple collision handling)
            Vector3 previousPosition = this.position;

            // Move the ball for this frame
            this.position += velocity;

            // Apply drag to the ball's velocity
            // Slow it down for the next fram
            velocity *= drag;

                // If this wall intersects with our ball...
                if (other.GetHitBox().Intersects(this.GetHitBox()))
                {
                    // We collided
                    // Move back to the stored position
                    this.position = previousPosition;

                    // Get bounding boxes for easy access
                    BoundingBox thisHitBox = this.GetHitBox();
                    BoundingBox otherHitBox = other.GetHitBox();

                    // Calculate the collision depth

                    // Get distance between the centres of the player and other
                    Vector3 centerOther = otherHitBox.Min + (otherHitBox.Max - otherHitBox.Min) / 2.0f;
                    Vector3 centerThis = thisHitBox.Min + (thisHitBox.Max - thisHitBox.Min) / 2.0f;
                    Vector3 distance = centerOther - centerThis;

                    // Minimum distance these need to be to NOT intersect
                    // AKA if they are AT LEAST this far away in either direction, they are NOT touching
                    Vector3 minDistance = (otherHitBox.Max - otherHitBox.Min) / 2.0f + (thisHitBox.Max - thisHitBox.Min) / 2.0f;

                    if (distance.X < 0)
                    {
                        minDistance.X = -minDistance.X;
                    }

                    if (distance.Y < 0)
                    {
                        minDistance.Y = -minDistance.Y;
                    }

                    if (distance.Z < 0)
                    {
                        minDistance.Z = -minDistance.Z;
                    }

                    // Collision depth = how much over the minimum distance are we in each direction
                    Vector3 depth = minDistance - distance;

                    // With collision depth, we can now tell which axis (x, y, z) arw we colliding on
                    // Now we just need to get the plane representing the play of collision in that axis

                    // Get the corners of the hitbox in an array
                    Vector3[] corners = otherHitBox.GetCorners();

                    // Smallest depth = direction of collision

                    // Variables to hold our plane points
                    // (Remember a plane can be represented by any two vectors on that plane)
                    Vector3 planeVector1;
                    Vector3 planeVector2;

                    if (Math.Abs(depth.Z) > Math.Abs(depth.X))
                    {
                        // x smaller, so we're colliding on the x axis  

                        // choose east plane ( positive x side) for collision
                        // (both result in same calculation so no need to check)
                        // which side of the x axis we're colliding from

                        planeVector1 = corners[0] - corners[5];
                        planeVector2 = corners[6] - corners[5];

                    }
                    else
                    {
                        // Z is smaller, so we're colliding on the z axis

                        // Choose the North plane 

                        // Two vector from north plane:
                        planeVector1 = corners[1] - corners[0];
                        planeVector2 = corners[3] - corners[0];
                    }

                    // Get nomral vector, use the cross product
                    Vector3 normal = Vector3.Cross(planeVector1, planeVector2);

                    // Normal vectors are supposed to be unit vectors(length 1)
                    //  so we need to normalize() function to change them to lenght 1
                    normal.Normalize();

                    // Reflect the player's velocity off the surface using normal
                    // This uses the dot product internally      
                    velocity = Vector3.Reflect(velocity, normal);
                }
            

            ///////////////////////////////////////////////////////////////////  
            // END TASK 3 CODE
            ///////////////////////////////////////////////////////////////////  
        }
        // ------------------
        public virtual void Draw(Camera cam, DirectionalLightSource light)
        {

        }
        // ------------------
    }
}
